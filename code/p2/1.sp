* Ehsan Jahangirzadeh 810194554
* CA5 VLSI

**** Load libraries ****
.prot
.inc 'Customized PTM 65nm.txt' 
.unprot

**** Parameters ****
.param	Lmin=65nm
.temp 25

**** Source Voltage ****
Vdd Vdd GND DC  1.1v
Va 	A	GND	dc PULSE(0V 1V 20ns 0ns 0ns 75ns 150ns)
Vb 	B	GND	dc PULSE(0V 1V 60ns 0ns 0ns 75ns 150ns)
**** Subkits ****

**** Circuit ****
M1 			out 	A		VDD			VDD			pmos 	w='2*Lmin'	L=Lmin 
M2 			out		B		VDD			VDD			pmos	w='2*Lmin'	L=Lmin

M3 			out		A		x			  GND			nmos 	w='2*Lmin'	L=Lmin
M4 			x		  B 	GND 		GND			nmos 	w='2*Lmin'	L=Lmin
**** Analysis ****
.tran 1ps 200ns

**** Measurements ****
.op
.MEASURE TRAN tpd 
+	TRIG V(B)  	VAL='V(vdd)/2'		CROSS=1
+   TARG V(out) VAL='V(vdd)/2'   	FALL=1

.end