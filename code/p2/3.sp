* Ehsan Jahangirzadeh 810194554
* CA5 VLSI

**** Load libraries ****
.prot
.inc 'Customized PTM 65nm.txt' 
.unprot

**** Parameters ****
.param	Lmin=65nm
.param Out_T_DLY=6
.param prd = Out_T_DLY
.temp 25
.vec "VectorTest.txt"

**** Source Voltage ****
Vdd Vdd GND DC  1.1v
**** Subkits ****

**** Circuit ****
M1 			out 	A		VDD			VDD			pmos 	w='2*Lmin'	L=Lmin 
M2 			out		B		VDD			VDD			pmos	w='2*Lmin'	L=Lmin

M3 			out		A		x			  GND			nmos 	w='2*Lmin'	L=Lmin
M4 			x		  B 	GND 		GND			nmos 	w='2*Lmin'	L=Lmin
**** Analysis ****
.tran 1ps 1ns

**** Measurements ****
.op
.measure TRAN iavg AVG i(vdd) FROM=0 TO=50ps
.measure TRAN power PARAM='iavg*V(vdd)'

.end