* Ehsan Jahangirzadeh 810194554
* CA5 VLSI

**** Load libraries ****
.prot
.inc 'Customized PTM 32nm.txt' 
.unprot

**** Parameters ****
.param Out_T_DLY=4
.param prd = Out_T_DLY
.param	Lmin=32nm
.temp 25
.vec "VectorTest.txt"

**** Source Voltage ****
Vdd Vdd GND DC  0.9v
Va 	A	GND	DC	1v
Vb 	B 	GND	DC 	1V
**** Subkits ****

**** Circuit ****
M1 			out 	A		VDD			VDD			pmos 	w='2*Lmin'	L=Lmin 
M2 			out		B		VDD			VDD			pmos	w='2*Lmin'	L=Lmin

M3 			out		A		x			  GND			nmos 	w='2*Lmin'	L=Lmin
M4 			x		  B 	GND 		GND			nmos 	w='2*Lmin'	L=Lmin
**** Analysis ****
.tran 1ps 1ns

**** Measurements ****
.op
.measure TRAN iavg1 AVG i(M1) FROM=0 TO=50ps
.measure TRAN iavg2 AVG i(M2) FROM=0 TO=50ps

.measure TRAN power PARAM='(-iavg1-iavg2)*V(vdd)'

.end